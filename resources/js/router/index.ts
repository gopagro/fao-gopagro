import { createRouter, createWebHistory, Router } from 'vue-router'
import { useAuthStore } from '@/store'
import middlewarePipeline from './middlewarePipeline'
import routes from './routes'

const router: Router = createRouter({
    history: createWebHistory(import.meta.env.VITE_BASE_URL),
    routes,
    scrollBehavior: (to: any, from: any , savedPosition: any): any => {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    },
})

router.beforeEach((to, from, next) => {
    const storeAuth = useAuthStore()
    const middleware: any = to.meta.middleware
    const context = { to, from, next, storeAuth }

    if (!middleware) return next()

    middleware[0]({
      ...context,
      next: middlewarePipeline(context, middleware, 1),
    })
})

export default router;
