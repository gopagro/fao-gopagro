import Home from '@/pages/Home.vue'
import Unauthorized from '@/pages/Unauthorized.vue'
import NotFound from '@/pages/NotFound.vue'
import { auth, guest, admin } from '@/middleware'
import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Home',
        meta: { middleware: [guest], layout: 'empty' },
        component: Home,
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        meta: { middleware: [auth] },
        component: () => import('@/pages/Dashboard.vue'),
    },
    {
    path: '/user',
    name: 'User',
    meta: { middleware: [auth] },
    component: () => import('@/pages/users/User.vue'),
  },
  {
    path: '/users',
    name: 'Users',
    meta: { middleware: [auth], can: 'users-list' },
    component: () => import('@/pages/users/Users.vue'),
  },
  {
    path: '/users/create',
    name: 'UsersCreate',
    meta: { middleware: [auth], can: 'users-create' },
    component: () => import('@/pages/users/UsersCreate.vue'),
  },
  {
    path: '/users/:id',
    name: 'UsersId',
    meta: { middleware: [auth], can: 'users-list' },
    component: () => import('@/pages/users/UsersId.vue'),
    children: [
      {
        path: 'edit',
        name: 'UserEdit',
        meta: {
          middleware: [auth],
          can: (to: any, from: any, can: any) => {
            return can('users-update', { id: +to.params.id })
          }
        },
        component: () => import('@/components/users/UserEditForm.vue'),
      },
    ],
  },
  {
    path: '/roles',
    name: 'Roles',
    meta: { middleware: [auth], can: 'roles-list' },
    component: () => import('@/pages/users/Roles.vue'),
  },
  {
    path: '/roles/create',
    name: 'RolesCreate',
    meta: { middleware: [auth], can: 'roles-create' },
    component: () => import('@/pages/users/RolesCreate.vue'),
  },
  {
    path: '/roles/:id',
    name: 'RolesId',
    meta: { middleware: [auth], can: 'roles-list' },
    component: () => import('@/pages/users/RolesId.vue'),
    children: [
      {
        path: 'edit',
        name: 'RoleEdit',
        meta: { middleware: [auth], can: 'roles-update' },
        component: () => import('@/components/users/RoleEditForm.vue'),
      },
    ],
  },
  {
    path: '/about',
    name: 'About',
    meta: { middleware: [guest] },
    component: () => import('@/pages/About.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    meta: { middleware: [guest] },
    component: () => import('@/pages/Register.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    meta: { middleware: [guest], layout: 'empty' },
    component: () => import('@/pages/Login.vue'),
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    meta: { middleware: [guest], layout: 'empty' },
    component: () => import('@/pages/ResetPassword.vue'),
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    meta: { middleware: [guest] },
    component: () => import('@/pages/ForgotPassword.vue'),
  },
  {
    path: "/unauthorized",
    name: "Unauthorized",
    meta: { layout: 'empty' },
    component: Unauthorized,
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    meta: { layout: 'empty' },
    component: NotFound,
  }
]

export default routes;
