import Role from './role.model'

export default interface User {
  id?: string;
  name: string;
  email: string;
  avatar?: string;
  emailVerified?: boolean;
  roles?: Role[];
}
