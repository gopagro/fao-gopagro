import { User } from '.'

export default interface Message {
  id?: number;
  message: string;
  user?: User
  createdAt?: string;
}
