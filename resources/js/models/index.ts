export { default as Role } from "./role.model";
export { default as User } from "./user.model";
export { default as Message } from "./message.model";

export type Credential = {
  email: string;
  password: string;
};

export type RegisterData = {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
};
