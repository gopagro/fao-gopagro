export type TypeMeta = {
  next_page?: number;
  prev_page?: number;
  first_page?: number;
  last_page?: number;
}

export type TypeForm = {
  data: FormData;
}

export type TypeFileUpload = {
  file: File;
  endpoint: string;
}

export type TypeAvatar = {
  name: string;
  data: File;
}

export type UpdateUserPayload = {
  id: string;
  data: FormData;
}
