
import { MiddlewareProps } from "."

export default function (props: MiddlewareProps) {
  const loginQuery = { name: 'Login', query: { redirect: props.to.fullPath } }

  if (!props.store.user) {
    props.store
      .getAuthUser()
      .then(() => !props.store.user ? props.next(loginQuery) : props.next())
  } else {
    props.next()
  }
}
