import { MiddlewareProps } from "."

export default function (props: MiddlewareProps) {
  if (props.store.isAdmin) props.next()
  else props.next({ name: 'Unauthorized'})
}
