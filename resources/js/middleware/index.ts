export { default as auth } from './auth'
export { default as admin } from './admin'
export { default as guest } from './guest'

export type MiddlewareProps = {
  to?: any;
  next: any;
  store: any;
}

