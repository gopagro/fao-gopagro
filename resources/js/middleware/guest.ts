import { MiddlewareProps } from "."

export default function(props: MiddlewareProps) {
  const storageItem = window.localStorage.getItem('guest')

  if (storageItem === 'isNotGuest' && !props.store.user) {
    props.store.getAuthUser().then(() => {
      if (props.store.user) {
        props.next({ name: 'Dashboard' })
      } else {
        props.store.setGuest('isGuest')
        props.next()
      }
    })
  } else {
    props.next()
  }
}
