import type { ErrorResponse, ValidationError } from "@/composables/fetch";

export type ErrorFormated = string | Record<string, any> | undefined;

const errorMessage = "API Error, please try again!";

export default function (error: ErrorResponse): ErrorFormated {
  if (error instanceof Error) {
    if (error.name == "Fetch User") {
      return error.message;
    }
  }

  if (isValidationError(error)) {
    if (import.meta.env.NODE_ENV == "development") {
      console.error(error.message);
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);
    }

    return error.response.data.errors
  }

  if (import.meta.env.NODE_ENV == "development") {
    console.error(error.message);
  }

  return error.message ?? errorMessage;
}

function isValidationError(error: ErrorResponse): error is ValidationError {
  return !!(error as ValidationError).response
}

export function isErrorResponse(response: void | object | ErrorResponse): response is ErrorResponse {
  return !!(response as ErrorResponse)
}
