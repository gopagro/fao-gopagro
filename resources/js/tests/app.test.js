import { describe, expect, it } from 'vitest'

describe('tests', () => {
  it('should works', () => {
    expect(1 + 1).toEqual(2)
  })

  it('Calculates Math', () => {
    expect(Math.sqrt(4)).toBe(2)
    expect(Math.sqrt(144)).toBe(12)
    expect(Math.sqrt(2)).toBe(Math.SQRT2)
  })
})
