import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import acl from './acl'
import sentry from '@/services/sentry'
import DashboardLayout from '@/components/layouts/DashboardLayout.vue'
import EmptyLayout from '@/components/layouts/EmptyLayout.vue'

const app = createApp(App)
const pinia = createPinia()

sentry(app, router)

app
    .use(router)
    .use(pinia)
    .use(acl)
    .component('default-layout', DashboardLayout)
    .component('empty-layout', EmptyLayout)

app.mount('#app')
