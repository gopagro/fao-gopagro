import useFetch, { FetchResponse } from './fetch'
import type { Credential, RegisterData, User } from '@/models'
import type { ErrorResponse } from './fetch'
import { LocationQueryValue } from 'vue-router';

const fetchService = useFetch();

type ResetPasswordPayload = {
  email: string;
  password: string;
  password_confirmation: string;
  toke: string;
}

type UpdatePasswordPayload = {
  current_password: string;
  password: string;
  password_confirmation: string;
}

export default function useAuthService() {

    return {
      async login(credentials: Credential): Promise<FetchResponse<User> | ErrorResponse> {
        await fetch("/sanctum/csrf-cookie")
        return (fetchService.post("/login", credentials)) as Promise<FetchResponse<User> | ErrorResponse>;
      },
      async register(data: RegisterData): Promise<FetchResponse<User> | ErrorResponse> {
        await fetch("/sanctum/csrf-cookie")
        return (fetchService.post("/register", data)) as Promise<FetchResponse<User> | ErrorResponse>;
      },
      logout(): void {
        fetchService.post("/logout", null);
      },
      async getAuthUser(): Promise<FetchResponse<User> | ErrorResponse> {
        await fetch("/sanctum/csrf-cookie")
        return (fetchService.get('/user')) as Promise<FetchResponse<User> | ErrorResponse>;
      },
      async forgotPassword(payload: {email: string}): Promise<FetchResponse<void> | ErrorResponse> {
        await fetch("/sanctum/csrf-cookie")
        return (fetchService.post('/forgot-password', payload)) as Promise<FetchResponse<void> | ErrorResponse>;
      },
      async resetPassword(payload: ResetPasswordPayload): Promise<FetchResponse<void> | ErrorResponse> {
        await fetch("/sanctum/csrf-cookie")
        return (fetchService.post('/reset-password', payload)) as Promise<FetchResponse<void> | ErrorResponse>;
      },
      updatePassword(payload: UpdatePasswordPayload): Promise<FetchResponse<void> | ErrorResponse> {
        return (fetchService.patch('/users/password', payload)) as Promise<FetchResponse<void> | ErrorResponse>;
      },
      sendVerification(payload: any): Promise<FetchResponse<void> | ErrorResponse> {
        return (fetchService.post('/email/verification-notification', payload)) as Promise<FetchResponse<void> | ErrorResponse>;
      },
      updateUser(payload: User): Promise<FetchResponse<void> | ErrorResponse> {
        return (fetchService.patch('/users/profile-information', payload)) as Promise<FetchResponse<void> | ErrorResponse>;
      },
    };
}
