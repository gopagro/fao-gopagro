import useFetch, { FetchResponse, ErrorResponse } from './fetch'
import type { Message } from '@/models'

const fetchService = useFetch();

export default function useMessageService() {
  return {
    getMessages(page: number): Promise<FetchResponse<Message[]> | ErrorResponse> {
      return fetchService.get(`/messages/?page=${page}`) as Promise<FetchResponse<Message[]> | ErrorResponse>;
    },
    postMessage(payload: Message): Promise<FetchResponse<Message> | ErrorResponse> {
      return fetchService.post('/messages', payload) as Promise<FetchResponse<Message> | ErrorResponse>
    },
    paginateMessages(link: string): Promise<FetchResponse<Message[]> | ErrorResponse> {
      return fetchService.get(link) as Promise<FetchResponse<Message[]> | ErrorResponse>
    },
  }
}

