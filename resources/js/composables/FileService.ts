import useFetch, { FetchResponse, ErrorResponse } from './fetch'
import type { Message } from '@/models'

const fetchService = useFetch();

export default function useFileService() {
  return {
    uploadFile(payload: any): Promise<FetchResponse<void> | ErrorResponse> {
      return fetchService.post(payload.endpoint, payload.file) as Promise<FetchResponse<void> | ErrorResponse>;
    }
  }
}

