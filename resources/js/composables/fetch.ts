import { useAuthStore } from "@/store";

export const API_URL: string = import.meta.env.VITE_API_URL
const csrfToken: string = document.querySelector<HTMLElement>('meta[name="csrf-token"]')?.getAttribute('content') || ''

export type FetchResponse<T> = {
  data: T;
  meta?: object;
  links?: object;
};

export type ValidationError = {
  message: string;
  response: {
    data: {errors: Array<object>};
    status: number;
    headers: object;
  };
  config?: object;
}

export type ErrorResponse = Error | ValidationError;

export default function useFetch() {

    return {
        async get(url: string): Promise<FetchResponse<object> | ErrorResponse> {
            try {
                const response: Response = await fetch(`${API_URL}${url}`, {
                    headers: {
                        'Accept': 'application/json',
                    },
                    credentials: 'include',
                })
                if (!response.ok) throw Error('Network error!')
                const data: FetchResponse<object> = await response.json()
                return data
            } catch (err: unknown) {
              return err as Error
            }
        },
        async post(url: string, body: object|null): Promise<FetchResponse<object> | ErrorResponse> {
            try {
                const response: Response = await fetch(`${API_URL}${url}`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': csrfToken
                    },
                    credentials: 'include',
                    body: JSON.stringify(body),
                })
                if (!response.ok) throw Error('Network error!')
                const data: FetchResponse<object> = await response.json()
                return data
            } catch (err: unknown) {
              if (!(err instanceof Error)) throw err
              return err
            }
        },
        async patch(url: string, body: object): Promise<FetchResponse<object> | ErrorResponse> {
            try {
                const response: Response = await fetch(`${API_URL}${url}`, {
                    method: 'PATCH',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': csrfToken
                    },
                    credentials: 'include',
                    body: JSON.stringify(body),
                })
                if (!response.ok) throw Error('Network error!')
                const data: FetchResponse<object> = await response.json()
                return data
            } catch (err: unknown) {
              if (!(err instanceof Error)) throw err
              return err
            }
        },
        async delete(url: string): Promise<void | ErrorResponse> {
            try {
                const response: Response = await fetch(`${API_URL}${url}`, {
                    method: 'DELETE',
                    credentials: 'include',
                })
                if (!response.ok) throw Error('Network error!')
            } catch (err: unknown) {
              if (!(err instanceof Error)) throw err
              return err
            }
        },
    }
}

function fetch(resource: string, config: object) {
  const { fetch: originalFetch } = window;

  const autStore = useAuthStore()

  try {
    return originalFetch(resource, config);
  } catch (err: any) {
    if ([401, 419].includes(err.response.status)
      && !!autStore.user
      && !autStore.guest ) {
      autStore.logout()
    }
    return Promise.reject(err)
  }
}
