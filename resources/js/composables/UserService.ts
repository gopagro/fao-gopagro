import useFetch, { ErrorResponse, FetchResponse } from "./fetch";
import { User, Role } from "@/models";
import { TypeForm, UpdateUserPayload } from "@/types";

const fetchService = useFetch();

export default function useUserService() {
  return {
    getUser(userId: number): Promise<FetchResponse<User> | ErrorResponse> {
      return fetchService.get(`/users/${userId}`) as Promise<FetchResponse<User> | ErrorResponse>
    },
    getUsers(page: number): Promise<FetchResponse<User[]> | ErrorResponse> {
      return fetchService.get(`/users/?page=${page}`) as Promise<FetchResponse<User[]> | ErrorResponse>
    },
    getRoles(): Promise<FetchResponse<Role[]> | ErrorResponse>{
      return fetchService.get('/roles/all') as Promise<FetchResponse<Role[]> | ErrorResponse>;
    },
    postUser(payload: TypeForm): Promise<FetchResponse<User> | ErrorResponse> {
      return fetchService.post('/users', payload) as Promise<FetchResponse<User> | ErrorResponse>;
    },
    updateUser(userId: string, payload: UpdateUserPayload): Promise<FetchResponse<User> | ErrorResponse> {
      return fetchService.post(`/users/${userId}`, payload) as Promise<FetchResponse<User> | ErrorResponse>;
    },
    deleteUser(userId: string): Promise<FetchResponse<void> | ErrorResponse> {
      return fetchService.delete(`/users/${userId}`) as Promise<FetchResponse<void> | ErrorResponse>;
    },
    paginatedUsers(link: string): Promise<FetchResponse<User[]> | ErrorResponse> {
      return fetchService.get(link) as Promise<FetchResponse<User[]> | ErrorResponse>;
    }
  }
}
