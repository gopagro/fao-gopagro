import useFetch, { ErrorResponse, FetchResponse } from "./fetch";
import { Role } from "@/models";

const fetchService = useFetch();

export default function useRoleService() {
  return {
    getRole(roleId: number): Promise<FetchResponse<Role> | ErrorResponse> {
      return fetchService.get(`/roles/${roleId}`) as Promise<
        FetchResponse<Role> | ErrorResponse
      >;
    },
    getRoles(page: number): Promise<FetchResponse<Role[]> | ErrorResponse> {
      return fetchService.get(`/roles/?page=${page}`) as Promise<
        FetchResponse<Role[]> | ErrorResponse
      >;
    },
    getPermissions(): Promise<FetchResponse<string[]> | ErrorResponse> {
      return fetchService.get("/permissions") as Promise<
        FetchResponse<string[]> | ErrorResponse
      >;
    },
    postRole(payload: Role): Promise<FetchResponse<Role> | ErrorResponse> {
      return fetchService.post("/roles", payload) as Promise<FetchResponse<Role> | ErrorResponse>;
    },
    updateRole(roleId: number, payload: Role): Promise<FetchResponse<Role> | ErrorResponse> {
      return fetchService.patch(`/roles/${roleId}`, payload) as Promise<FetchResponse<Role> | ErrorResponse>;
    },
    deleteRole(roleId: number): Promise<void | ErrorResponse> {
      return fetchService.delete(`/roles/${roleId}`);
    },
    paginatedRoles(link: string): Promise<FetchResponse<Role[]> | ErrorResponse> {
      return fetchService.get(link) as Promise<FetchResponse<Role[]> | ErrorResponse>;
    }
  };
}
