import { defineStore } from 'pinia'

export type MainState = {
  open: boolean;
}

const useMainStore = defineStore('main', {
  state: (): MainState => ({
    open: false,
  })
})

export default useMainStore
