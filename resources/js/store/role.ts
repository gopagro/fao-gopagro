import { defineStore } from 'pinia'
import { FetchResponse, ErrorResponse } from '@/composables/fetch';
import { Role, Message } from '@/models';
import { TypeMeta } from '@/types';
import formatError, { ErrorFormated, isErrorResponse } from '@/utils/formatError';
import useRoleService from '@/composables/RoleService'
import router from '@/router'

const roleService = useRoleService()

export type RoleState = {
  roles: Role[],
  role: Role | null,
  meta: TypeMeta | null | undefined,
  links: object | null | undefined,
  loading: boolean,
  error: ErrorFormated | null,
  message: Message | null,
  allPermissions: string[],
}

const useRoleStore = defineStore('role', {
  state: (): RoleState => ({
    roles: [],
    role: null,
    meta: null,
    links: null,
    loading: false,
    error: null,
    message: null,
    allPermissions: [],
  }),
  actions: {
    async getRoles(page: number): Promise<void> {
      this.loading = true
      const response = await roleService.getRoles(page)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.setPaginated(response)
      }
      this.loading = false
    },
    async getPermissions(): Promise<void> {
      this.loading = true
      const response = await roleService.getPermissions()
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.allPermissions = response.data
      }
      this.loading = false
    },
    async getRole(payload: number): Promise<void> {
      this.loading = true
      const response = await roleService.getRole(payload)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.role = response.data
      }
      this.loading = false
    },
    async postRole(payload: Role): Promise<void> {
      this.loading = true
      const response = await roleService.postRole(payload)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.roles = [...this.roles, response.data]
        this.putMessage({message: 'Role created successfully'})
      }
      this.loading = false
    },
    async updateRole(payload: Role): Promise<void> {
      if (payload.id) {
        this.loading = true
        const response = await roleService.updateRole(payload.id, payload)

        if (isErrorResponse(response)) {
          this.error = formatError(response)
        } else {
          this.roles = this.roles.map(
            (role: Role) => role.id === response.data.id ? response.data : role
          )
          this.role = response.data
          this.putMessage({message: 'Role Updated'})
        }
        this.loading = false
      }
    },
    async deleteRole(): Promise<void> {
      this.loading = true
      if (!!this.role && !!this.role.id && this.role.name !== 'super-admin') {
        const response = await roleService.deleteRole(this.role.id)
        if (isErrorResponse(response)) {
          this.error = formatError(response)
        } else {
          this.roles = this.roles.filter((role: Role) => role.id !== this.role?.id)
          this.putMessage({message: 'Role deleted successfully'})
        }
        this.loading = false
        router.push({ name: 'Roles' })
      } else {
        this.error = formatError(Error("Can't delete your own role!"))
      }
    },
    async paginateRoles(link: string) {
      this.loading = true
      const response = await roleService.paginatedRoles(link)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.setPaginated(response)
      }
      this.loading = false
    },
    putMessage(payload: Message) {
          this.message = payload
          setTimeout(() => this.message = null, 5000)
    },
    putError(payload: ErrorResponse) {
          this.error = formatError(payload)
          setTimeout(() => this.error = null, 5000)
    },
    setPaginated(response: FetchResponse<Role[]>) {
      this.roles = response.data
      this.meta = response.meta
      this.links = response.links
      this.loading = false
    },
  },
})

export default useRoleStore
