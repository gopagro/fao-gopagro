import { defineStore } from 'pinia'
import formatError, { ErrorFormated, isErrorResponse } from '@/utils/formatError';
import { FetchResponse } from '@/composables/fetch';
import useMessageService from '@/composables/MessageService'
import { User, Role, Message } from '@/models'
import { TypeMeta } from '@/types'

const messageService = useMessageService();

export type MessageState = {
  messages: Message[];
  meta: TypeMeta | null | undefined;
  links: object | null | undefined;
  loading: boolean;
  error: ErrorFormated | null;
}

const useMessageStore = defineStore('message', {
  state: (): MessageState => ({
    messages: [],
    meta: null,
    links: null,
    loading: false,
    error: null,
  }),
  actions: {
    async getMessages(page: number): Promise<void> {
      this.loading = true
      const response = await messageService.getMessages(page)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.setPaginated(response)
      }
      this.loading = false
    },
    async postMessage(payload: Message): Promise<void> {
      this.loading = true
      const response = await messageService.postMessage(payload)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.messages = [...this.messages, response.data]
      }
      this.loading = false
    },
    async paginateMessages(link: string): Promise<void> {
      this.loading = true
      const response = await messageService.paginateMessages(link)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.setPaginated(response)
      }
      this.loading = false
    },
    setPaginated(response: FetchResponse<Message[]>) {
      this.messages = response.data
      this.meta = response.meta
      this.links = response.links
    },
  },
})


export default useMessageStore
