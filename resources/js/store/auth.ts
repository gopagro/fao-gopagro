import { defineStore } from 'pinia'
import formatError, { ErrorFormated, isErrorResponse } from '@/utils/formatError'
import router from '@/router'
import useAuthService from '@/composables/AuthService'
import type { User, Role } from '@/models'
import type { ErrorResponse } from '@/composables/fetch'

const authService = useAuthService()

export type AuthState = {
  user: User | null;
  loading: boolean;
  error: ErrorFormated | null;
}

const useAuthStore = defineStore('auth', {
  state: (): AuthState => ({
    user: null,
    loading: false,
    error: null,
  }),
  getters: {
    isAdmin: (state: AuthState) => state.user?.roles?.some((role: Role) => role.name === 'super-admin'),
    guest: (): boolean => {
      const storageItem = window.localStorage.getItem('guest')
      return !!storageItem && storageItem === 'isGuest'
    },
  },
  actions: {
    logout(): void {
      authService.logout()
      this.user = null
      this.setGuest('isGuest')
      if (router.currentRoute.value.name !== 'Login') {
        router.push({ name: 'Login' })
      }
    },
    async getAuthUser(): Promise<void> {
      this.loading = true
      const response = await authService.getAuthUser()
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.user = response.data
      }
      this.loading = false
    },
    setGuest(value: string) {
      window.localStorage.setItem('guest', value)
    },
  },
})

export default useAuthStore
