import { defineStore } from 'pinia'
import formatError, { ErrorFormated, isErrorResponse } from '@/utils/formatError';
import { FetchResponse, ErrorResponse } from '@/composables/fetch';
import useUserService from '@/composables/UserService'
import router from '@/router'
import { User, Role, Message } from '@/models'
import { TypeMeta, TypeForm, UpdateUserPayload } from '@/types';
import { useAuthStore } from '.'

const userService = useUserService();
const authStore = () => useAuthStore();

export type UserState = {
  users: User[];
  user: User | null;
  meta: TypeMeta | null | undefined;
  links: object | null | undefined;
  loading: boolean;
  error: ErrorFormated | null;
  message: Message | null;
  allRoles: Role[];
};

const useUserStore = defineStore('user', {
  state: (): UserState => ({
    users: [],
    user: null,
    meta: null,
    links: null,
    loading: false,
    error: null,
    message: null,
    allRoles: [],
  }),
  actions: {
    async getUsers(page: number): Promise<void> {
      this.loading = true
      const response = await userService.getUsers(page)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.setPaginated(response)
      }
      this.loading = false
    },
    async getRoles(): Promise<void> {
      this.loading = true
      const response = await userService.getRoles()
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.allRoles = response.data
      }
      this.loading = false
    },
    async getUser(payload: number): Promise<void> {
      this.loading = true
      const response = await userService.getUser(payload)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.user = response.data
      }
      this.loading = false
    },
    async postUser(payload: TypeForm): Promise<void> {
      this.loading = true
      const response = await userService.postUser(payload)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.users = [...this.users, response.data]
        this.putMessage({message: 'User created successfully'})
      }
      this.loading = false
    },
    async updateUser(payload: UpdateUserPayload): Promise<void> {
      this.loading = true
      const response = await userService.updateUser(payload.id, payload)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.users = [...this.users, response.data]
        this.users = this.users.map(
          (user: User) => user.id === response.data.id ? response.data : user
        )
        this.user = response.data
        this.putMessage({message: 'User updated successfully'})
      }
      this.loading = false
    },
    async deleteUser(): Promise<void> {
      if (!!this.user && !!authStore().user && this.user.id !== authStore().user?.id) {
        const response = await userService.deleteUser(this.user.id as string)
        if (isErrorResponse(response)) {
          this.error = formatError(response)
        } else {
          this.users = this.users.filter((user: User) => user.id !== this.user?.id)
          this.putMessage({message: 'User deleted successfully'})
        }
        this.loading = false
        router.push({ name: 'Users' })
      } else {
        this.error = formatError(Error("Can't delete your own user!"))
      }
    },
    async paginateUsers(link: string) {
      this.loading = true
      const response = await userService.paginatedUsers(link)
      if (isErrorResponse(response)) {
        this.error = formatError(response)
      } else {
        this.setPaginated(response)
      }
      this.loading = false
    },
    putMessage(payload: Message) {
          this.message = payload
          setTimeout(() => this.message = null, 5000)
    },
    putError(payload: ErrorResponse) {
          this.error = formatError(payload)
          setTimeout(() => this.error = null, 5000)
    },
    setPaginated(response: FetchResponse<User[]>) {
      this.users = response.data
      this.meta = response.meta
      this.links = response.links
    },
  },
})


export default useUserStore
