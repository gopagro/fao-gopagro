import * as Sentry from '@sentry/vue'
import { BrowserTracing } from '@sentry/tracing'
import { Router } from 'vue-router'
import { App } from 'vue'

export default (app: App, router: Router): void => {
    Sentry.init({
        app,
        dsn: import.meta.env.VITE_SENTRY_DSN,
        integrations: [
            new BrowserTracing({
            routingInstrumentation: Sentry.vueRouterInstrumentation(router),
            tracingOrigins: [...import.meta.env.VITE_SENTRY_TRACING_ORIGINS.split(','), '/^\//'],
            }),
        ],
        tracesSampleRate: import.meta.env.VITE_SENTRY_TRACES_SAMPLE_RATE,
    });
}
