stages:
  - build
  - test
  - deploy

image: registry.gitlab.com/gopagro/fao-gopagro

# Pick zero or more services to be used on all builds.
# Only needed when using a docker container to run your tests in.
# Check out: https://docs.gitlab.com/ee/ci/services/index.html
services:
  - name: postgis/postgis:14-3.2-alpine
    alias: pgsql

variables:
  POSTGRES_DB: fao_gopagro
  POSTGRES_USER: sail
  POSTGRES_PASSWORD: password
  XDEBUG_MODE: develop,debug,coverage

.init_ssh: &init_ssh |
  eval $(ssh-agent -s)
  echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh
  [[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

composer:
  stage: build
  script:
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    # curl -sS https://getcomposer.org/installer | php
    # php composer.phar install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.example .env
    - php artisan key:generate
    - php artisan config:cache
    # Run database migrations.
    - php artisan migrate
    # Run database seed
    - php artisan db:seed

  artifacts:

    # (Optional) Give it an expiration date,
    # after that period you won't be able to
    # download them via the UI anymore.
    expire_in: 1 day

    # Define what to output from the job.
    paths:
      - vendor/
      - .env

  cache:
    # The variable CI_COMMIT_REF_SLUG
    # refers to the slug of the branch.
    # For example: `master` for the master branch.
    # We use the `composer` suffix to avoid conflicts with
    # the `npm` cache that we'll define next.
    key: ${CI_COMMIT_REF_SLUG}-composer

    # Define what to cache.
    paths:
      - vendor/

npm:
  # Same stage as `composer` so that they run in parallel.
  stage: build

  # Cache the `node_modules` folder
  # using the `npm` suffix on the key.
  cache:
    key: ${CI_COMMIT_REF_SLUG}-npm
    paths:
      - node_modules/

  # Install and compile.
  script:
      - npm install
      - npm run production

  # Provide the other jobs of the pipeline with
  # the node dependencies and the compiled assets.
  artifacts:
    expire_in: 1 day
    paths:
      - node_modules/
      - public/build/

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml

phpunit:
  stage: test
  dependencies:
    - composer
    - npm
  script:
    # run laravel tests
    - php vendor/bin/phpunit --coverage-text --colors=never --coverage-cobertura=coverage/php-cobertura.xml --log-junit=junit/php-report.xml

  coverage: '/^\s*Lines:\s*\d+.\d+\%/'

  artifacts:
    when: always
    paths:
      - coverage/php-cobertura.xml
      - junit/php-report.xml
    reports:
      junit: junit/php-report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage/php-cobertura.xml

vitest:
  stage: test
  dependencies:
    - npm
  script:
    # run javascript tests
    - npx vitest run --reporter=junit --outputFile junit/js-report.xml --coverage
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'
  artifacts:
    when: always
    paths:
      - coverage/coverage-final.json
      - junit/js-report.xml
    reports:
      junit: junit/js-report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage/coverage-final.json

codestyle:
  stage: test
  script:
    - php vendor/bin/pint --test

staging:
  stage: deploy
  script:
    # Reuse a template by calling `*reference`
    - *init_ssh
    - dpl --provider=heroku --app=$HEROKU_APP --api-key=$HEROKU_API_KEY
  environment:
    name: staging
    url: https://fao-gopagro-stagging.herokuapp.com/
  only:
    - main

production:
  stage: deploy
  script:
    # Reuse a template by calling `*reference`
    - *init_ssh
    - dpl --provider=heroku --app=$HEROKU_APP --api-key=$HEROKU_API_KEY
  environment:
    name: production
    url: https://fao-gopagro.herokuapp.com/
  # Do not run automatically.
  # Wait for a human to click on play.
  when: manual
  only:
    - main
